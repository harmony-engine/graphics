// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    vk::SurfaceFormatKHR choose_swap_format(const std::vector<vk::SurfaceFormatKHR>& formats) {
        for (const auto& format : formats) {
            if (format.format == vk::Format::eB8G8R8A8Srgb && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
                return format;
        }
        return formats[0];
    }

    vk::PresentModeKHR choose_swap_present(const std::vector<vk::PresentModeKHR>& presents) {
        for (const auto& present : presents) {
            if (present == vk::PresentModeKHR::eMailbox)
                return present;
        }
        return vk::PresentModeKHR::eFifo;
    }

    vk::Extent2D choose_swap_extent(const vk::SurfaceCapabilitiesKHR& capabilities, GLFWwindow* window) {
        if (capabilities.currentExtent.width != UINT32_MAX)
            return capabilities.currentExtent;
        else {
            int width, height;
            glfwGetFramebufferSize(window, &width, &height);

            return vk::Extent2D {
                std::clamp(
                    static_cast<uint32_t>(width),
                    capabilities.minImageExtent.width,
                    capabilities.maxImageExtent.width
                ),
                std::clamp(
                    static_cast<uint32_t>(height),
                    capabilities.minImageExtent.height,
                    capabilities.maxImageExtent.height
                )
            };
        }
    }

    void PipelineContainer::create_swapchain() {
        auto format = choose_swap_format(physical_device.getSurfaceFormatsKHR(*surface));
        auto capabilities = physical_device.getSurfaceCapabilitiesKHR(*surface);
        auto extent = choose_swap_extent(capabilities, window);
        auto indices = QueueFamilyIndices(physical_device, *surface);
        auto families = indices.unique_families();

        swapchain.swapchain = device->createSwapchainKHRUnique(vk::SwapchainCreateInfoKHR {
            { },
            *surface,
            capabilities.maxImageCount > 0 && capabilities.minImageCount >= capabilities.maxImageCount
                ? capabilities.maxImageCount
                : capabilities.minImageCount + 1,
            format.format,
            format.colorSpace,
            extent,
            1,
            vk::ImageUsageFlagBits::eColorAttachment,
            indices.families().size() == indices.unique_families().size()
                ? vk::SharingMode::eConcurrent
                : vk::SharingMode::eExclusive,
            families,
            capabilities.currentTransform,
            vk::CompositeAlphaFlagBitsKHR::eOpaque,
            choose_swap_present(physical_device.getSurfacePresentModesKHR(*surface)),
            VK_TRUE,
            VK_NULL_HANDLE
        });

        swapchain.images = device->getSwapchainImagesKHR(*swapchain.swapchain);
        swapchain.image_format = format.format;
        swapchain.extent = extent;
    }
}
