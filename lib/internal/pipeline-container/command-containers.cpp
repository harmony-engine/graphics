// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_command_pool() {
        command_pool = device->createCommandPoolUnique(vk::CommandPoolCreateInfo {
            { }, QueueFamilyIndices(physical_device, *surface).graphics_family.value()
        });
    }

    void PipelineContainer::create_command_buffers() {
        command_buffers = device->allocateCommandBuffersUnique(vk::CommandBufferAllocateInfo {
            *command_pool,
            vk::CommandBufferLevel::ePrimary,
            static_cast<uint32_t>(swapchain.framebuffers.size())
        });

        for (size_t i = 0; i < command_buffers.size(); i++) {
            auto& command_buffer = command_buffers[i];

            command_buffer->begin(vk::CommandBufferBeginInfo {
                {}, nullptr
            });

            std::vector<vk::ClearValue> clear_values {
                vk::ClearValue { vk::ClearColorValue { std::array<float, 4> {
                    0.0f, 0.0f, 0.0f, 1.0f }
                }}
            };

            command_buffer->beginRenderPass(vk::RenderPassBeginInfo {
                *render_pass,
                *swapchain.framebuffers[i],
                vk::Rect2D { {0, 0}, swapchain.extent },
                clear_values
            }, vk::SubpassContents::eInline);
            {
                command_buffer->bindPipeline(vk::PipelineBindPoint::eGraphics, *graphics_pipeline);
                command_buffer->draw(3, 1, 0, 0);
            }
            command_buffer->endRenderPass();
            command_buffer->end();
        }
    }
}
