// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_render_pass() {
        std::vector<vk::AttachmentDescription> color_attachments {
            vk::AttachmentDescription{
                { },
                swapchain.image_format,
                vk::SampleCountFlagBits::e1,
                vk::AttachmentLoadOp::eClear,
                vk::AttachmentStoreOp::eStore,
                vk::AttachmentLoadOp::eDontCare,
                vk::AttachmentStoreOp::eDontCare,
                vk::ImageLayout::eUndefined,
                vk::ImageLayout::ePresentSrcKHR
            }
        };

        std::vector<vk::AttachmentReference> color_attachment_refs {
            vk::AttachmentReference {
                0, vk::ImageLayout::eColorAttachmentOptimal
            }
        };

        std::vector<vk::SubpassDescription> subpass {
            vk::SubpassDescription {
                { },
                vk::PipelineBindPoint::eGraphics,
                { },
                color_attachment_refs
            }
        };

        std::vector<vk::SubpassDependency> dependencies {
            vk::SubpassDependency {
                VK_SUBPASS_EXTERNAL,
                0,
                vk::PipelineStageFlagBits::eColorAttachmentOutput,
                vk::PipelineStageFlagBits::eColorAttachmentOutput,
                vk::AccessFlags { },
                vk::AccessFlagBits::eColorAttachmentWrite
            }
        };

        render_pass = device->createRenderPassUnique(vk::RenderPassCreateInfo {
            { },
            color_attachments,
            subpass,
            dependencies
        });
    }
}
