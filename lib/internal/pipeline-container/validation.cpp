// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"

#include <iostream>


namespace Harmony::Graphics {
    PFN_vkCreateDebugUtilsMessengerEXT  create_messenger;
    PFN_vkDestroyDebugUtilsMessengerEXT destroy_messenger;

    static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
        vk::DebugUtilsMessageSeverityFlagsEXT severity,
        vk::DebugUtilsMessageTypeFlagsEXT type,
        const vk::DebugUtilsMessengerCallbackDataEXT* data,
        void* user_data
    ) {
        std::cerr << "Vulkan Validation: " << data->pMessage << std::endl;
        return VK_FALSE;
    }

    void PipelineContainer::enable_validation() {
        create_messenger = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(
            instance->getProcAddr("vkCreateDebugUtilsMessengerEXT")
        );
        destroy_messenger = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(
            instance->getProcAddr("vkDestroyDebugUtilsMessengerEXT")
        );

        debug_messenger = instance->createDebugUtilsMessengerEXTUnique(
            vk::DebugUtilsMessengerCreateInfoEXT {
                { },
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
                vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
                vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
                vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
                reinterpret_cast<PFN_vkDebugUtilsMessengerCallbackEXT>(debug_callback)
            }
        );
    }

    bool PipelineContainer::check_validation_support() {
        auto available_layers = vk::enumerateInstanceLayerProperties();

        return std::all_of(layers.begin(), layers.end(), [&available_layers](char const* name) {
            return std::find_if(available_layers.begin(), available_layers.end(), [&name]( vk::LayerProperties const & property) {
                return strcmp(property.layerName, name) == 0;
            }) != available_layers.end();
        });
    }
}

VKAPI_ATTR VkResult VKAPI_CALL vkCreateDebugUtilsMessengerEXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT* create_info,
        const VkAllocationCallbacks* allocator,
        VkDebugUtilsMessengerEXT* messenger
) {
    return Harmony::Graphics::create_messenger(instance, create_info, allocator, messenger);
}

VKAPI_ATTR void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT(
        VkInstance instance,
        VkDebugUtilsMessengerEXT messenger,
        const VkAllocationCallbacks* allocator
) {
    Harmony::Graphics::destroy_messenger(instance, messenger, allocator);
}
