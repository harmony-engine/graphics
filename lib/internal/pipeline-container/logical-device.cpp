// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_logical_device() {
        auto indices = QueueFamilyIndices(physical_device, *surface);

        float priority = 1;
        std::vector<vk::DeviceQueueCreateInfo> queues;
        for (auto index : indices.unique_families()) {
            queues.push_back(vk::DeviceQueueCreateInfo(
                { },
                index,
                1, &priority
            ));
        };

        vk::PhysicalDeviceFeatures features;
        features.dualSrcBlend = VK_TRUE;

        device = physical_device.createDeviceUnique(vk::DeviceCreateInfo {
            { },
            queues,
            layers,
            extensions,
            &features
        });

        graphics_queue = device->getQueue(indices.graphics_family.value(), 0);
        present_queue = device->getQueue(indices.present_family.value(), 0);
    }
}
