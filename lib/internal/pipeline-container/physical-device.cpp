// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    int32_t rate_device_type(vk::PhysicalDeviceType type) {
        switch (type) {
            case vk::PhysicalDeviceType::eCpu:
                return 0;
            case vk::PhysicalDeviceType::eOther:
                return 500;
            case vk::PhysicalDeviceType::eIntegratedGpu:
                return 2000;
            case vk::PhysicalDeviceType::eVirtualGpu:
                return 3500;
            case vk::PhysicalDeviceType::eDiscreteGpu:
                return 10000;
        }
    }

    void PipelineContainer::pick_physical_device() {
        int32_t best_score = -2147483648;
        for (const auto& device : instance->enumeratePhysicalDevices()) {
            auto properties = device.getProperties();
            auto features = device.getFeatures();
            auto available_extensions = device.enumerateDeviceExtensionProperties();

            int32_t this_score
                = rate_device_type(properties.deviceType)
                + (features.geometryShader ? 0 : -1048576)
                + (features.dualSrcBlend ? 0 : -1048576)
                + (QueueFamilyIndices(device, *surface).complete() ? 0 : -1048576)
                + (std::all_of(extensions.begin(), extensions.end(), [&available_extensions](char const* name) {
                    return std::find_if(available_extensions.begin(), available_extensions.end(), [&name](vk::ExtensionProperties props) {
                        return strcmp(props.extensionName, name) == 0;
                    }) != available_extensions.end();
                }) ? 0 : -1048576)
                + (device.getSurfaceFormatsKHR(*surface).empty() ? -1048576 : 0)
                + (device.getSurfacePresentModesKHR(*surface).empty() ? -1048576 : 0)
                + properties.limits.maxImageDimension2D;

            if (this_score > best_score) {
                physical_device = device;
                best_score = this_score;
            }
        }
        if (best_score < 0)
            throw std::runtime_error("No suitable GPUs found");
    }
}
