// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"

#include <core/application.h>

#include <stdint.h>


namespace Harmony::Graphics {
    std::vector<const char*> PipelineContainer::get_extensions() {
        uint32_t glfw_extension_count = 0;
        const char** glfw_extensions;
        glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);
        std::vector<const char*> extensions(glfw_extensions, glfw_extensions + glfw_extension_count);

        if (validation_enabled)
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

        return extensions;
    }

    void PipelineContainer::create_instance() {
        if (validation_enabled && !check_validation_support())
            throw std::runtime_error("Validation enabled, but is not supported");

        vk::ApplicationInfo app_info {
            Harmony::application_data.name.c_str(),
            VK_MAKE_VERSION(
                Harmony::application_data.version.major,
                Harmony::application_data.version.minor,
                Harmony::application_data.version.patch
            ),
            engine_name.c_str(),
            VK_API_VERSION_1_2
        };

        auto extensions = get_extensions();
        instance = vk::createInstanceUnique(vk::InstanceCreateInfo {
            {} ,
            &app_info,
            layers,
            extensions
        });
    }
}
