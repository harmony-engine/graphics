// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::init_window() {
        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);

        window = glfwCreateWindow(mode->width/2, mode->height/2, Harmony::application_data.name.c_str(), nullptr, nullptr);
    }

    void PipelineContainer::create_surface() {
        VkSurfaceKHR raw_surface;
        if (glfwCreateWindowSurface(*instance, window, nullptr, &raw_surface) != VK_SUCCESS)
            throw std::runtime_error("Failed to create window surface");
        surface = vk::UniqueSurfaceKHR(raw_surface, *instance);
    }
}
