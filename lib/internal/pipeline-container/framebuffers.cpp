// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_framebuffers() {
        swapchain.framebuffers.clear();
        for (auto& image_view : swapchain.image_views) {
            std::vector<vk::ImageView> attachments {
                *image_view
            };

            swapchain.framebuffers.push_back(device->createFramebufferUnique(vk::FramebufferCreateInfo {
                { },
                *render_pass,
                attachments,
                swapchain.extent.width,
                swapchain.extent.height,
                1
            }));
        }
    }
}
