// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"

#include <unordered_set>


namespace Harmony::Graphics {
    PipelineContainer::QueueFamilyIndices::QueueFamilyIndices(vk::PhysicalDevice device, vk::SurfaceKHR& surface) {
        int i = 0;
        for (const auto& family : device.getQueueFamilyProperties()) {
            if (family.queueFlags & vk::QueueFlagBits::eGraphics)
                graphics_family = i;
            if (device.getSurfaceSupportKHR(i, surface))
                present_family = i;
            if (complete()) return;
            i++;
        }
    }

    bool PipelineContainer::QueueFamilyIndices::complete() {
        return graphics_family.has_value() && present_family.has_value();
    }

    std::vector<uint32_t> PipelineContainer::QueueFamilyIndices::families() {
        return std::vector<uint32_t> {
            *graphics_family,
            *present_family
        };
    }

    std::vector<uint32_t> PipelineContainer::QueueFamilyIndices::unique_families() {
        std::unordered_set<uint32_t> set {
            *graphics_family,
            *present_family
        };

        return std::vector(set.begin(), set.end());
    }
}
