// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"

#include <core/resource.h>


namespace Harmony::Graphics {
    void PipelineContainer::create_graphics_pipeline() {
        auto vert_shader_data = Harmony::load_resource<uint32_t>("harmony_graphics", "shaders/sprite.vert.spv");
        auto frag_shader_data = Harmony::load_resource<uint32_t>("harmony_graphics", "shaders/sprite.frag.spv");
        auto vert_shader = device->createShaderModuleUnique(vk::ShaderModuleCreateInfo {
            { }, vert_shader_data
        });
        auto frag_shader = device->createShaderModuleUnique(vk::ShaderModuleCreateInfo {
            { }, frag_shader_data
        });

        std::vector<vk::PipelineShaderStageCreateInfo> shader_stages {
            vk::PipelineShaderStageCreateInfo {
                { },
                vk::ShaderStageFlagBits::eVertex,
                *vert_shader,
                "main"
            },
            vk::PipelineShaderStageCreateInfo {
                { },
                vk::ShaderStageFlagBits::eFragment,
                *frag_shader,
                "main"
            }
        };

        vk::PipelineVertexInputStateCreateInfo vertex_input_info {
            vk::PipelineVertexInputStateCreateFlags { }
        };

        vk::PipelineInputAssemblyStateCreateInfo input_assembly {
            { },
            vk::PrimitiveTopology::eTriangleList,
            VK_FALSE
        };

        vk::PipelineTessellationStateCreateInfo tessellation_state {
            { },
            0
        };

        std::vector<vk::Viewport> viewports {
            vk::Viewport {
                0.0f, 0.0f,
                static_cast<float>(swapchain.extent.width), static_cast<float>(swapchain.extent.height),
                0.0f, 1.0f
            }
        };

        std::vector<vk::Rect2D> scissors {
            vk::Rect2D {vk::Offset2D {0, 0}, swapchain.extent}
        };

        vk::PipelineViewportStateCreateInfo viewport_state {
            { },
            viewports, scissors
        };

        vk::PipelineRasterizationStateCreateInfo rasterizer {
            {},
            VK_FALSE, VK_FALSE,
            vk::PolygonMode::eFill,
            vk::CullModeFlagBits::eBack,
            vk::FrontFace::eClockwise,
            VK_FALSE,
            0.0f, 0.0f, 0.0f,
            1.0f
        };

        vk::PipelineMultisampleStateCreateInfo multisampling {
            { },
            vk::SampleCountFlagBits::e1,
            VK_FALSE
        };

        std::vector<vk::PipelineColorBlendAttachmentState> color_blend_attachments {
            vk::PipelineColorBlendAttachmentState {
                VK_TRUE,
                vk::BlendFactor::eSrcAlpha,
                vk::BlendFactor::eOneMinusSrc1Alpha,
                vk::BlendOp::eAdd,
                vk::BlendFactor::eOne,
                vk::BlendFactor::eZero,
                vk::BlendOp::eAdd,
                vk::ColorComponentFlagBits::eR |
                vk::ColorComponentFlagBits::eG |
                vk::ColorComponentFlagBits::eB |
                vk::ColorComponentFlagBits::eA
            }
        };

        vk::PipelineColorBlendStateCreateInfo color_blending {
            { },
            VK_FALSE,
            vk::LogicOp::eCopy,
            color_blend_attachments
        };

        pipeline_layout = device->createPipelineLayoutUnique(vk::PipelineLayoutCreateInfo {
            vk::PipelineLayoutCreateFlags { }
        });

        graphics_pipeline = device->createGraphicsPipelineUnique(VK_NULL_HANDLE, vk::GraphicsPipelineCreateInfo {
            { },
            shader_stages,
            &vertex_input_info,
            &input_assembly,
            &tessellation_state,
            &viewport_state,
            &rasterizer,
            &multisampling,
            nullptr,
            &color_blending,
            nullptr,
            *pipeline_layout,
            *render_pass,
            0
        }).value;
    }
}
