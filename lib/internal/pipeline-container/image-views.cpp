// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_image_views() {
        swapchain.image_views.clear();
        for (auto& image : swapchain.images) {
            swapchain.image_views.push_back(device->createImageViewUnique(vk::ImageViewCreateInfo {
                { },
                image,
                vk::ImageViewType::e2D,
                swapchain.image_format,
                vk::ComponentMapping { },
                vk::ImageSubresourceRange {
                    vk::ImageAspectFlagBits::eColor,
                    0, 1, 0, 1
                }
            }));
        }
    }
}
