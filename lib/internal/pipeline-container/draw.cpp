// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"

#include <limits>


namespace Harmony::Graphics {
    void PipelineContainer::draw() {
        std::vector<vk::Fence> wait_fences_first { *synchronisation.in_flight[current_frame] };
        device->waitForFences(wait_fences_first, VK_TRUE, std::numeric_limits<uint64_t>::max());

        uint32_t image_index;
        try {
            image_index = device->acquireNextImageKHR(
                *swapchain.swapchain,
                std::numeric_limits<uint64_t>::max(),
                *synchronisation.image_available[current_frame]
            ).value;
        } catch (const std::runtime_error&) {
            return create_full_pipeline();
        }

        if (synchronisation.images_in_flight[image_index] != vk::Fence(nullptr)) {
            std::vector<vk::Fence> wait_fences_second { synchronisation.images_in_flight[current_frame] };
            device->waitForFences(wait_fences_second, VK_TRUE, std::numeric_limits<uint64_t>::max());
        }
        synchronisation.images_in_flight[image_index] = *synchronisation.in_flight[current_frame];

        std::vector<vk::Semaphore> wait_semaphores { *synchronisation.image_available[current_frame] };
        std::vector<vk::PipelineStageFlags> wait_stages = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
        std::vector<vk::CommandBuffer> s_command_buffers { *command_buffers[image_index] };
        std::vector<vk::Semaphore> signal_semaphores { *synchronisation.render_finished[current_frame] };

        std::vector<vk::SubmitInfo> submits {
            vk::SubmitInfo {
                wait_semaphores,
                wait_stages,
                s_command_buffers,
                signal_semaphores
            }
        };

        device->resetFences(wait_fences_first);
        graphics_queue.submit(submits, *synchronisation.in_flight[current_frame]);

        std::vector<vk::SwapchainKHR> swapchains { *swapchain.swapchain };
        std::vector<uint32_t> image_indices { image_index };
        vk::Result present_result;
        try {
            present_result = present_queue.presentKHR(vk::PresentInfoKHR{
                signal_semaphores,
                swapchains,
                image_indices
            });
        } catch (const std::runtime_error&) {
            create_full_pipeline();
        }
        if (present_result == vk::Result::eSuboptimalKHR)
            create_full_pipeline();

        current_frame = (current_frame + 1) % max_frames_in_flight;
    }
}
