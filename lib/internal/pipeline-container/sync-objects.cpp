// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#include "graphics/pipeline-container.h"


namespace Harmony::Graphics {
    void PipelineContainer::create_sync_objects() {
        synchronisation.image_available.clear();
        synchronisation.render_finished.clear();
        synchronisation.in_flight.clear();
        synchronisation.images_in_flight.clear();

        for (size_t i = 0; i < max_frames_in_flight; i++) {
            synchronisation.image_available.push_back(device->createSemaphoreUnique(vk::SemaphoreCreateInfo {
                vk::SemaphoreCreateFlags { }
            }));
            synchronisation.render_finished.push_back(device->createSemaphoreUnique(vk::SemaphoreCreateInfo {
                vk::SemaphoreCreateFlags { }
            }));
            synchronisation.in_flight.push_back(device->createFenceUnique(vk::FenceCreateInfo {
                vk::FenceCreateFlagBits::eSignaled
            }));
        }
        synchronisation.images_in_flight.resize(swapchain.images.size(), vk::Fence(nullptr));
    }
}
