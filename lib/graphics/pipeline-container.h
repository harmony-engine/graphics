// Copyright (C) 2020 Evan La Fontaine
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.


#pragma once

#include <vulkan/vulkan.hpp>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <optional>

#include <core/system.h>
#include <core/application.h>


namespace Harmony::Graphics {
    system PipelineContainer {
    public:
        void initialise() override;
        void decommission() override;

    private:
        void draw();

        void create_graphics_instance();
        void init_window();
        void create_instance();
        void enable_validation();

        void pick_physical_device();
        void create_logical_device();
        void create_surface();
        void create_command_pool();

        void create_full_pipeline();
        void create_swapchain();
        void create_image_views();
        void create_render_pass();
        void create_graphics_pipeline();

        void create_framebuffers();
        void create_command_buffers();
        void create_sync_objects();

        std::vector<const char*> get_extensions();
        bool check_validation_support();

        GLFWwindow* window;
        vk::UniqueInstance instance;
        vk::UniqueDebugUtilsMessengerEXT debug_messenger;
        vk::UniqueSurfaceKHR surface;

        vk::PhysicalDevice physical_device;
        vk::UniqueDevice device;

        vk::Queue graphics_queue;
        vk::Queue present_queue;

        struct {
            vk::UniqueSwapchainKHR swapchain;
            std::vector<vk::Image> images;
            vk::Format image_format;
            vk::Extent2D extent;

            std::vector<vk::UniqueImageView> image_views;
            std::vector<vk::UniqueFramebuffer> framebuffers;
        } swapchain;

        vk::UniqueRenderPass render_pass;
        vk::UniquePipelineLayout pipeline_layout;
        vk::UniquePipeline graphics_pipeline;

        vk::UniqueCommandPool command_pool;
        std::vector<vk::UniqueCommandBuffer> command_buffers;

        struct {
            std::vector<vk::UniqueSemaphore> image_available;
            std::vector<vk::UniqueSemaphore> render_finished;
            std::vector<vk::UniqueFence> in_flight;
            std::vector<vk::Fence> images_in_flight;
        } synchronisation;

        size_t current_frame = 0;


        const std::string engine_name = "Harmony";
        const bool validation_enabled = true;
        const std::vector<const char*> layers = {
            "VK_LAYER_KHRONOS_validation"
        };
        const std::vector<const char*> extensions = {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };
        const size_t max_frames_in_flight = 2;

        struct QueueFamilyIndices {
        public:
            QueueFamilyIndices(vk::PhysicalDevice device, vk::SurfaceKHR& surface);
            bool complete();
            std::vector<uint32_t> families();
            std::vector<uint32_t> unique_families();

            std::optional<uint32_t> graphics_family;
            std::optional<uint32_t> present_family;
        };
    };
}
