function(shaders names)
    get_property(COMMAND_ID GLOBAL PROPERTY HPROP_CURRENT_COMMAND_ID)
    get_property(PROJECT_SOURCE_DIR GLOBAL PROPERTY HPROP_COMMAND_SOURCE_DIR_${COMMAND_ID})
    get_property(PROJECT_BINARY_DIR GLOBAL PROPERTY HPROP_COMMAND_BINARY_DIR_${COMMAND_ID})
    foreach(SHADER IN LISTS names)
        add_custom_command(
            OUTPUT ${PROJECT_BINARY_DIR}/${SHADER}.spv
            DEPENDS ${PROJECT_SOURCE_DIR}/${SHADER}
            COMMAND glslc ${PROJECT_SOURCE_DIR}/${SHADER} -o ${PROJECT_BINARY_DIR}/${SHADER}.spv
        )
    endforeach()
endfunction()
